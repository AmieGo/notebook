Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-10-13T17:09:40+02:00

====== 2- 常用API-实列的生产和销毁 ======
Created Dienstag 13 Oktober 2020

{{./pasted_image.png}}


{{./pasted_image001.png}}{{./pasted_image002.png}}

{{./pasted_image003.png}}
{{./pasted_image004.png}}


=== 注意-具体编程实现： ===

1- 创建空物体，并绑定生成实列的cs文件（ex：预制体实列）。
	1- 注意的问题： 如何在cs脚本中引入unity3d的预制体cube，并对cube进行操作？
	
		1- 在cs脚本中申明一个 **public GameObject** procube的对象， 此时此procube名会在unity3d创建的空对象的cs脚本中多出一个属性。如下
				{{./pasted_image006.png}}
				
		2- 然后直接将预制体拖拽到框中，然后就可以在cs脚本中对预制体进行控制了

					{{./pasted_image007.png}}

1-  为**预制体创建脚本**，用于控制那些所有用预制体生成的实列 （**此时脚本绑定在预制体上的**，对所有生成的预制体都有效）
	
	1-  将预制体拖拽到场景中
	2- 将脚本添加到cube上，然后点击apply，然后删除cube，之后预制体就绑定了此脚本
	3- 在脚本中设置对于所有生成的预制体都有的属性，**定时销毁自己。**
	
		{{./pasted_image008.png}}













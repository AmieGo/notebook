Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-10-01T20:24:43+08:00

====== 14-教室开发案列 ======
Created 星期四 01 十月 2020

{{.\pasted_image.png}}

{{.\pasted_image001.png}}
{{.\pasted_image002.png}}
{{.\pasted_image003.png}}
{{.\pasted_image004.png}}


1-  首先将门绑定到一个空物体上（空物体作为parent）
2- 将空物体中心移动合适的位置。
3- 再创建一个空物体，并添加collider将整个门包裹起来，并勾选 Is Trigger属性
4- 创建Trigger.cs 文件，添加给3中的空物体。逻辑是：一旦OnTriggerEnter中接收到“student”的物体后，就开门，OntriggerExit()中接收到“student”的物体后，就关门。


=== 注意： 在Trigger.cs中调用其他cs中的函数（EX：door.cs 中的Openthedoor（）函数）。 ===
			{{.\pasted_image005.png}}

		**1- 首先再door.cs中将，要被其他类中（Trigger.cs中）调用的函数设置为 public类型（必须添加）。**
				{{.\pasted_image006.png}}
		
		**2- 在 Trigger.cs 中引入door中的类（组件）。**
				
				注：CS文件也被当做是一个对象的组件
				
				{{.\pasted_image007.png}}
		
		{{.\pasted_image012.png}}    {{.\pasted_image013.png}}
		
		
		**3- 注意Trigger的触发（注意box collider的范围）。**
			
			{{.\pasted_image009.png}}							{{.\pasted_image008.png}}
			此时“student” 触发triggerEnter，门开。			然后“student”并没有移动，此时由于门的运动导致
																								触发了triggerExit，然后门又马上关上了，然后触发triggerEnter，门又打开。所以就在这里不停的开门关门。



Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2021-03-12T10:47:24+01:00

====== 0- G++ ======
Created 星期五 12 三月 2021


=== 0- 输出GNU 的默认include-path，Library-path，和Libraries ===
	
	**# cpp -v**
			......
			#include **"..."** search starts here:               
			#include **<...>** search starts here:
			 /usr/lib/gcc/x86_64-pc-cygwin/6.4.0/include
			 /usr/include
			 /usr/lib/gcc/x86_64-pc-cygwin/6.4.0/../../../../lib/../include/w32api
			
	**# g++ -v func func.cpp     (编译时显示用到的库等)**


=== 0- 1 Utilities for Examining the Compiled Files ===
	
	1- "ldd" Utility - **List Dynamic-Link Libraries**
	
		**# ldd func       or 		# ldd func.exe**
		
		{{./pasted_image002.png}}
		


=== 1- 对于用G++构建项目时，其搜索路径规则如下 ===
	https://zhuanlan.zhihu.com/p/159432310
	
	**7-1 关于<> 和 “ ”**
	
		#include <stdio.h>
		#include “module.h”
		
		如果你引用的头文件是**标准库的头文件或官方路径下**的头文件，一般使用尖括号**<>**包含；
		如果你使用的头文件是**自定义的或项目中的头文件**，一般使用双引号**""**包含



	**7-2 G++的compile → link--> build 过程(4个过程)**

			{{./pasted_image.png}}
			
			**源码：**
			
			// hello.cpp
			#include <iostream>
			using namespace std;
			 
			int main() {
			   cout << "Hello, world!" << endl;
			   return 0;
			}
			
			
			**命令：** gcc -o hello.exe hello.c 的执行顺序如下
			
					1- **Pre-processing:** via the GNU C Preprocessor (cpp.exe), which includes the headers (#include) and expands the macros (#define).
								
								**#  cpp hello.c > hello.i        （生成hello.i）**
								
					2- **Compilation:** The compiler compiles the pre-processed source code into assembly code for a specific processor
					
								**# gcc -S hello.i       (生成hello.s)**
					**# g++ -S hello.i       (生成hello.s)**

The -S option specifies to produce assembly code, instead of object code. The resultant assembly file is **"hello.s".**
					
					3- **Assembly:** The assembler (as.exe) converts the assembly code into machine code in the object file "hello.o".

								**# as -o hello.o hello.s  (生成hello.o)**
					
					4-**Linker:** Finally, the linker (ld.exe) **links the object code with the library** code to produce an executable file "hello.exe".
					
								**# ld -o hello.exe hello.o ...libraries...   （生成hello.exe）**


=== 2- G++ 编译一个cpp文件（G++的原料就是.cpp or .c文件？） ===
			
			源码：
					// hello.cpp
					#include <iostream>
					using namespace std;
					 
					int main() {
					   cout << "Hello, world!" << endl;
					   return 0;
					}
					
			
			**1- 最直接的编译命令：**
			
					**# g++ -o hello hello.cpp**
					# chmod a+x hello
					# ./hello
			
			**2- 更多参数选项：**
			
					**# g++ -Wall -g -o Hello  Hello.cpp**
					
					**-o:** specifies the output executable filename.
					**-Wall:** prints "all" Warning messages.
					**-g:** generates additional symbolic debuggging information for use with gdb debugger.
					
			
			**3-** 在2命令中（The above command **compile the source file into object file and link with other object files and system libraries** into executable in one step.）但是我们只想单独编译目前的文件，稍后再做链接。则用 **-c** 命令。

					
					# g++ -c -Wall -g Hello.cpp       (Compile-only with -c option)
					# g++ -g -o Hello.exe Hello.o    (Link object file(s) into an executable)
					
					**-c:** Compile into object file "Hello.o". By default, the object file has the same name as the source file with extension of ".o" (**there is no need to specify -o option**). No linking with other object files or libraries.
					
					**Linking** is performed **when the input file are object files ".o" (instead of source file ".cpp" or ".c")**. GCC uses a separate linker program **(called ld.exe) to perform the linking.**
					
			
			**4- 编译多个cpp文件时。**
			
				 一步到位法：
						# g++ -o myprog file1.cpp file2.cpp 
						
				分别编译再链接方法：
						# g++ -c file1.cpp
						# g++ -c file2.cpp
						# g++ -o myprog file1.o file2.o

						[[./add.cpp]] 
						[[./add.h]] 
						[[./func.cpp]] 
						
						






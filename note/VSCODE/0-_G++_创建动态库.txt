Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2021-03-12T13:31:44+01:00

====== 0- G++ 创建动态库 ======
Created 星期五 12 三月 2021


https://renenyffenegger.ch/notes/development/languages/C-C-plus-plus/GCC/create-libraries/index


=== 1- 创建动态库（.so） ===
	
	**定义：** 程序运行的时候采取调用。
	**目的：** 编译并获得动态文件库(.so)后，则可以在自己的项目中调用动态库中的函数。
				只需要把静态库对应的头文件（.h）和(.a)文件拷贝到自己的工程目录中去，然后在自己的项目中#include“ ” 就好。
				在编译的时候通过 **-L, -l** 告诉compiler动态库位置，和静态库的名称。
				
				

=== 2- 创建步骤 ===

	step1： 创建自己的库文件(add.h, add.c, answer.h, answer.c), 并存放在目录**src/tq84/** 下

			[[./src/tq84/add.c]]
			 [[./src/tq84/add.h]] 
			[[./src/tq84/answer.c]] 
			[[./src/tq84/answer.h]]



	step2: 将常用函数库（step1中）编译成动态文件（其实就是将c,cpp文件里的**函数实现**编译成动态文件）。
				
				又因为（.so） 文件为(.o)文件的一个合集，所以我们可以分别去编译add.c, answer.c 生成对应的.o 文件。
				
				
				=== 注意： 这里是必须要使用参数 -fPIC去生成.o文件，否则在生成动态库的时候就会有错误 ===
				
				// object files for shared libraries need to be compiled as position independent
				// code (-fPIC) because they are mapped to any position in the address space.

				# gcc -c **-fPIC** src/tq84/add.c    -o bin/shared/add.o
				# gcc -c **-fPIC** src/tq84/answer.c -o bin/shared/answer.o
	
	step3： 使用-shared命令将(.o)文件打包为（.so）文件
				
				# gcc **-shared** bin/shared/add.o bin/shared/answer.o -o bin/shared/libtq84.so
				
	
	
	step4： 在自己的项目中使用动态文件（link statically）
	
				1- 需要将step3中生成的**（.so）文件**，和其对应的头文件**（header）**库拷贝到和自己工程同在的一个目录中（或自己的工程能找到的目录中）
				
				2- 创建自己的工程main.c 然后，加入想调用函数库的头文件（#include “ ”）

							[[./src/main.c]]
				
				3- 单独编译自己的main.c 文件生成 main.o文件
				
						**# gcc -c       src/main.c        -o bin/main.o**
						
				4- 将main.o 和动态库链接起来（linker）生成可执行文件。（不需要-llibtq84 只用-lltq84即可）
				
						// Note the order:
						//   -ltq84-shared needs to be placed AFTER main.c
						
						**#gcc  bin/main.o -Lbin/shared -ltq84 -o bin/use-shared-library**
						
						**-L：**The -L flag indicates (a non standard) directory where the libraries can be found.
						**-l ：**The -l flag indicates the name of the library**. Note, that it assumes the library to start with lib and end with .o** (so lib and .o must not be specified)
				
				**5- 运行生成的可执行文件  (此时是无法运行成功的，和静态编译的不同。)**
						
						# ./bin/statically-linked  （运行失败！！！！！！！！！！）

										
										{{./pasted_image.png}}
 

										**Note：因为生成的 .so 文件不在gcc默认的搜索路径里面，所以在编译的时候，compiler尝试去默认路径找libtq84.so,但是不存在。（2个处理办法，1- 将.so所在路径加入gcc搜索路径。2-将.so移动到gcc的默认搜索路径中去）**
										
										
				
				Solution1： 使用LD_LIBRARY_PATH 将.so添加到gcc的搜索路径
							
							1- 暂时引入动态库路径：
							 
								直接在终端输入： # **export LD_LIBRARY_PATH=$(pwd)/**
								然后再运行        ： # ./dynamic

								~**重新开一个终端的话失效**。
							
							
							2- 永久引入动态库路径：
								
								**修改**~/.bashrc
								
								**添加：**export LD_LIBRARY_PATH=/home/amie/C_plus_plus/C_func_test/create_static_lib:$LD_LIBRARY_PATH
								
								**激活：**# source ~/.bashrc.

					~重新开一个终端**不会失效。**


				Solution2：将生成的动态库移动到gcc默认的搜索路径中去：
				
						**# sudo mv bin/shared/libtq84.so /usr/lib**
						**# sudo chmod 755 /usr/lib/libtq84.so**
						
					~重新开一个终端**不会失效。**




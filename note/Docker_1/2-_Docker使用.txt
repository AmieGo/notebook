Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-10-20T09:10:07+08:00

====== 2- Docker使用 ======
Created 星期日 20 十月 2019


====== Docker的使用总结 ======


==== 1-安装Docker-EC看教程 ====

==== 2-首先要知道docker在整个系统中所处的位置和角色 ====
	   https://docs.docker.com/engine/docker-overview/
	
	=== 2.1 Docker Engine ===
		Docker Engine是一个CS应用，主要有以下的一些主要模块：
		 
		1-Server： docker deamon（是一个长期运行的守护进程）
		2-Rest API：客户端调用的接口，与Server交互
		3-Client： 命令
		CLI使用REST API控制或者与与Server进行交互，
		
		
		{{./pasted_image.png}}
		
		==== 2-2 Docker architecture ====
		Docker使用客户端-服务器架构。 Docker客户端与Docker守护程序进行对话，然后守护程序（Server）完成构建，运行和分发Docker容器的工作。
		
		{{~/Pictures/Selection_014.png}}
		
		
		**Docker daemon：**
		Docker  daemon（**dockerd**）侦听Docker API请求并管理Docker 对象，例如images, containers, networks, and volumes。守护程序还可以与其他守护程序通信以管理Docker服务。
		
		**The Docker client：**
		Docker client（**docker**）是许多Docker用户与Docker交互的主要方式。当您使用诸如**docker run**之类的命令时，客户端会将这些命令发送至**dockerd（Server）**，然后执行它们。 docker命令使用Docker API。 Docker客户端可以与多个守护程序通信。
		
		**Docker registries：**
		Docker registries存储Docker 镜像。 Docker Hub是任何人都可以使用的公共注册表，并且默认情况下，Docker已在Docker Hub上配置好了镜像。您甚至可以运行自己的   私人注册表。如果使用Docker Datacenter（DDC），则其中包括Docker Trusted Registry (DTR).。
		使用docker pull或docker run命令时，所需的镜像将从配置的注册表中提取。使用docker push命令时，会将映像推送到配置的注册表。
		
		命令解释：
		$ docker run -i -t ubuntu [[/bin/bash]]
		
		1- If you do not have the ubuntu image locally, Docker pulls it from your configured registry, as though you had run docker pull ubuntu manually.
		
		2- Docker creates a new container, as though you had run a docker container create command manually.
		
		3- Docker allocates a read-write filesystem to the container, as its final layer. This allows a running container to create or modify files and directories in its local filesystem.
		
		4- Docker creates a network interface to connect the container to the default network, since you did not specify any networking options. This includes assigning an IP address to the container. By default, containers can connect to external networks using the host machine’s network connection.
		
		5- Docker starts the container and executes /bin/bash. Because the container is running interactively and attached to your terminal (due to the -i and -t flags), you can provide input using your keyboard while the output is logged to your terminal.
		
		6- When you type exit to terminate the /bin/bash command, the container stops but is not removed. You can start it again or remove it.
		
	

=== Note：不加sudo运行docker$ sudo 启动docker  http://www.markjour.com/article/docker-no-root.html ===
			{{./pasted_image007.png}}
			
			{{./pasted_image008.png}}


		

===== 3-docker的运行逻辑 =====

1-首先安装好docker Engine 后，
2-我们就从私人或者共有的registry上拉去自己需要的镜像（操作系统或者环境），
3-然后我们再通过拉去的镜像（模板环境，ex：ubuntu16.04）创建自己的容器，容器是自己可以实实在在操作的对象，
4-然后进入容器进行项目的创建，库的安装等等，
5-然后再通过commit提交，否则在再通过ubuntu16.04去创建容器的时候，你在第4步中安装的库和工具都不会在新的容器中存在。

=== Note：Docker 使用GPU资源和cudnn库的原理（安装看链接2） ===
		需要什么样的cudnn和cuda配置的镜像可以直接去docker hub上去找，找到对应的命令然后拉去镜像：
		docker-hub:   https://hub.docker.com/r/elezar/caffe/
			安装nvidia-docker： https://github.com/NVIDIA/nvidia-docker

			1-原生的docker是不支持使用GPU的，所以NVIDIA就基于原生的Docker完成了一个NVIDIA Container Toolkit ， 这个工具允许用户创建和使用GPU加速器。此工具包含了 container runtime library and utilities to automatically configure containers to leverage NVIDIA GPUs. **注意：这个工具是基于原生的docker，所以必须要先安装原生的docker后再安装这个工具，以达到使用宿主机GPU的目的。**
		2-从docker-Hub中拉去镜像后，如果使用原生的docker去创建容器$ docker run -i -t ubuntu [[/bin/bash]] ，那**其创建的容器是无法使用GPU的（即使拉去的镜像是配置好了cudnn和cuda），可以通过nvidia-smi来验证**
		**通过NVIDIA-DOCKER创建的容器，**使用：$sudo docker run --name 容器名 --gpus all -it 镜像名 /bin/bash     则可以使用GPU进行加速。
																		   ex：  $sudo docker run --name test1 --gpus all -it ubuntu /bin/bash


===== 4-对容器的操作 =====

1-通过3中的命令创建好容器后，可以查看镜像，容器列表，启动容器，终止容器，重启容器，后台运行容器，导出和导入容器，删除容器，具体见思维导图。


===== 5-在docker中运行一个GPU项目，并保存为自己的镜像，提交到registry，然后拉去镜像二次开发流程。 =====

=== 1-根据项目拉去拉去合适cuda+cudnn 版本的镜像：  https://hub.docker.com/r/pytorch/pytorch/tags ===
EX: 为yolov3 pytorch-gpu版本创建docker环境。
安装pytorch-gpu-docker（1.3-cuda10.1-cudnn7-runtime）


**$ sudo docker pull pytorch/pytorch:1.3-cuda10.1-cudnn7-runtime**

**$ sudo docker images **

{{./pasted_image001.png}}


=== 2- 建立个人容器 ===
如果用原生的**$ sudo docker run -it pytorch/pytorch:1.3-cuda10.1-cudnn7-runtime**
那么进入容器后是无法使用gpu的。
必须要使用nvdia-docker进行创建:
**$** **sudo docker run --name test-pytorch1.3 --gpus all -it pytorch/pytorch:1.3-cuda10.1-cudnn7-runtime /bin/bash**

**$ sudo docker containers ls**

{{./pasted_image002.png}}


=== 3-将本地项目发送到指定的docker容器中(传输文件夹不用加 -r) ===
	$ sudo docker cp 本地文件路径 ID全称:容器路径
	$ sudo docker cp PyTorch-YOLOv3 40f4c2dfa098:/home
	
	{{./pasted_image004.png}}
	


=== 4-在docker中安装所需要的库，并commit到新镜像（不然再次利用模板镜像创建容器时不会有自己新安装的库） ===
1-进入指定的docker 容器安装matplotlib  

$ apt-get update   (若不能安装，请执行此命令)
$ sudo docker exec -it ID bash
$ pip install matplotlib

=== 5- 创建自己的镜像，使得新的镜像中包含有4步骤中安装的库。 ===

执行$ docker commit 容器ID 新的镜像名(假如是ping-vim) 就可以添加一个新的镜像
		**$ sudo docker commit 40f4c 1.3-cuda-cudnn-matplot**

{{./pasted_image006.png}}

可以看出新建的镜像名字为 1.3-cuda-cudnn-matplot（其中此镜像已经安装了matplotlib，且已经把项目源文件PyTorc-YOLOv3拷贝进了新建的docker容器中）。
然后基于此镜像创建新的容器test_matplot_1. 然后在新的容器中可以看到拥有源文件PyTorc-YOLOv3，且可以包含了包matplotlib可以直接调用。


===== 6-Docker hub 上传镜像&拉取镜像 =====

	6-1 注册Docker hub账号 https://www.docker.com/] 
	6-2 创建自己的镜像仓库 https://blog.csdn.net/Mr_sunrise/article/details/75146869 
	
	6-3 上传本地镜像到docker hub
	   1- 获取镜像ID--> 2-登录docker hub → 3-指定镜像上传到的仓库（tag）--> 4- push
			1-$  docker images
			2-$ docker login    
			  3-$ docker tag hello-world duxiaorui/test_pull_push
			  4-$ docker push duxiaorui/test_pull_push
		
	2-从个人docker hub上拉去镜像(要先删除本地的镜像？有待考证)
		1-获取镜像ID--> 2-登录docker hub → 3-pull  → 4-检查镜像
			  1-$  docker images
				2-$ docker login    
			  3-$ docker pull duxiaorui/test_pull_push:1.3-cuda-cudnn-matplot


===== 7-Docker + Pycharm 部署开发 =====
https://zhuanlan.zhihu.com/p/52827335  （有些配置是错误的）
条件： pycharm 使用专业版（破解看关注的微信公众号获得注册码：雨梦coder）
在服务器上安装docker 和 nvidia-docker（前面步骤已经安装好了）

		7-1 创建docker容器（需要配置好宿主机和容器的端口映射，因为要通过ssh进行访问容器）
		sudo nvidia-docker run -it -p [host_port]:[container_port](do not use 8888) --name:[container_name] [image_name] -v [container_path]:[host_path] /bin/bash
		
		ex：
		sudo nvidia-docker run -p 5592:5592 -p 5593:5593 -p 8022:22 --name="liuzhen_tf" -v ~/workspace/liuzhen/remote_workspace:/workspace/liuzhen/remote_workspace -it tensorflow/tensorflow:latest-gpu /bin/bash
		正确执行完之后，现在我们就处在新建的docker容器里了（端口映射，容器名，镜像和路径映射这些换成你自己的就行，但是一定要留一个端口映射到容器22端口，因为SFTP默认使用22端口）。
		
		7-2 配置SSH服务
			接着我们在刚刚新建的容器里配置SSH服务，首先安装openssh-server:

			$ apt update
			$ apt install -y openssh-server
			然后建立一个配置文件夹并进行必要的配置：（就是允许ssh可以root登录，建Frp内网穿透教程）
			ssh配置（必须要重置密码）
			如果希望用户能远程访问容器，除了网络配置之外，还需要修改一下（容器的）ssh配置。默认禁止root用户登录，容器创建默认用户也是root用户，里面有个ubuntu用户，未初始化。既然虚拟主机交给用户，即把root也给用户了，所以先设置允许root用户登录，如不需要可以让用户自行更改。
			1	vim /etc/ssh/sshd_config
			将其中的PermitRootLogin prohibit-password改为PermitRootLogin yes，以及ChallengeResponseAuthentication no改为ChallengeResponseAuthentication yes。
			然后为root用户设置密码：
			1	passwd root
			另外可以编辑ssh登录用户的欢迎信息，通过编辑/etc/update-motd.d/目录下的00-header和01-hepler-text中的内容即可完成。
			最后，重启ssh服务，
			1	/etc/init.d/ssh restart

			在服务器（宿主机）上（不是服务器的docker里）测试刚刚新建docker容器中哪个端口转发到了服务器的22端口：
			
			$ sudo docker port [your_container_name] 22
			# 如果前面的配置生效了，你会看到如下输出
			# 0.0.0.0:8022
			最后测试能否用SSH连接到远程docker：
			
			$ ssh root@[your_host_ip] -p 8022
			# 密码是你前面自己设置的
			到这里说明服务器的docker端已经完成配置。
			
			然后参照知乎上的进行安装配置就好。
			
			**Note1：如何配置在pycharm中配置远程docker的python解释器，比如要使用pytoch。**
				1-检查pytorch包的位置,以此来确定python解释器的位置
				 import torch
				 print(torch.__path__)
				// '/opt/conda/lib/python3.6/site-packages/torch'
				
			**Note2: 本地和容器中的同步显示：**
			Tools-->Deployment-->Browers Remote Host



===== 7- Docker中的图像显示到宿主机上 =====

(关于摄像头的暂时还没解决)

sudo nvidia-docker run -it -p 8022:22 --name='cv_cam_0' -v /home:/home/amie/docker/test/cv_cam -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --privileged -v /dev/video0:/dev/video0 pytorch/pytorch:1.3-cuda10.1-cudnn7-runtime /bin/bash   


**需要在主机终端输入:** $ xhost local:root
在运行docker中的显示图片的代码，就能正常显示了












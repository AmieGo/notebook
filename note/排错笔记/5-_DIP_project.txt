Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-10-23T16:13:37+02:00

====== 5- DIP project ======
Created 星期五 23 十月 2020


1- 在traning_and_eval部分

	1- 安装tensorflow-gpu-1.14.1 版本（conda 制定版本安装），可以暂时忽略Warning
	2- 使用numpy=1.16.1（导致了一个无法加载pkl问题，False。。？？）
	3- Debug调试代码， 通过键入字典代替终端命令行输入
	

3- 在Unity3d中对evaluation结过显示部分

		0 在u**nity2014.3.1f1**中新建项目（如果在unity3 5中建项目， dip中的cs脚本中一些函数无法使用（matrix那个函数））
		1- 安装JsonDotNet 插件 （JSON .NET For Unity）
		2- 导入smpl for unity的插件中**的smpl文件**夹到Asset下
		3- 创建新的scene（Scenes文件夹中）
		4- 按照unity中的设置，对模型参数进行重新设置（男女模型一起）
		
		**5- 在新场景中创建male模型（fmale），无法运行。因为在unity中，其Client的cs中写死了向Server（.py）请求的是male模型的参数。**
		
		6- 讲dip项目中live_demo文件夹下的所有cs文件拷贝到unity中的Scripts文件夹下，并将Visualize_data.cs 绑定在**m_avg**上（不是SMPL_f_unityDoubleBlends_lbs_10_scale5_207_v1.0.0.fbx 或者 m_avg_root上）
				{{./pasted_image001.png}}
		
		7-然后设置OURS文件夹路径，即evaluation后获得的*npz文件（我是直接把此文件和interface_server.py放在一个文件夹中了），此步是为了告诉server我需要server将那个数据回传给我。
						{{./pasted_image002.png}}


	unity中整体的目录结构：
	{{./pasted_image.png}}
	
	

Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-07-12T23:31:52+08:00

====== 2-Linux排错 ======
Created 星期日 12 七月 2020
ubuntu问题收集+ 解决方案


===== 1-Ubuntu 16.04 WPS 无法输入中文解决 =====

https://blog.csdn.net/duxu24/article/details/52711693  




===== 2- NVIDIA-SMI says it failed because it couldn't communicate with the NVIDIA driver =====
一段时间口nvidia-smi 发现驱动，是因为内核版本升高导致
https://blog.csdn.net/Siyuada/article/details/89423033 


===== 3- Host key verification failed =====

https://www.thegeekdiary.com/how-to-fix-the-error-host-key-verification-failed/ 


===== 4-shadowsocks-libev 服务端部署 =====
https://cokebar.info/archives/767   


===== 5-shadowsocks-libv 客户端部署(推荐使用) =====
https://www.xiaocoder.com/2018/08/17/ss-local-guide/ 



===== 6-shadowsocks-libev 服务端部署   + chrome——swithcomega + =====
 ssh -D 8838 root@serverip  + proxychains（如果没有办法了，最暴力的就是ssh -D 动态转发翻墙）

ssh -D 实现linux上的翻墙活动 + 全局代理！
6.1 首先参照4 在vps上部署好服务器，记住开的端口号（ex:8838）。2 在chrome中安装switchomega 并设置 socks5  127.0.0.1  8838。3-使用 ssh -D 8838 root@serverip  实现翻墙

%E7%84%B6%E5%90%8E%E6%8C%89%E7%85%A7https://github.com/naseeihity/LearnToLearn/issues/7  设置配置文件为socks5 127.0.0.1 8838 实现全局代理



===== 7- conda中opencv无法读入视频 解决方式： =====



conda install -c conda-forge ffmpeg
conda install -c conda-forge opencv


===== 8- 修复Grub2 boot loader =====

[[.\Grub_loader.docx]]




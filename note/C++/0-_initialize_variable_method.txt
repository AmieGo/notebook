Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2021-05-07T18:32:00+02:00

====== 0- initialize variable method ======
Created 星期五 07 五月 2021

1- static initialization 
2- dynamic initialization


====== 1- C ======

	{{./pasted_image.png}}

	method3:
	
	int a, b;
	a = 10;
	b=10;
{{./pasted_image001.png}}
	{{./pasted_image002.png}}
	


==== 2-  C++ ====
	{{./pasted_image003.png}}
	{{./pasted_image004.png}}
	{{./pasted_image005.png}}
	{{./pasted_image006.png}}
	
==== 3- new operator ====
https://www.geeksforgeeks.org/new-and-delete-operators-in-cpp-for-dynamic-memory/ 

	1- 
	{{./pasted_image007.png}}


	2-

	{{./pasted_image008.png}}
	
	
	{{./pasted_image009.png}}
	{{./pasted_image010.png}}
	


==== 4- delete operator ====

	1-  释放一组内存

		{{./pasted_image012.png}}
		{{./pasted_image011.png}}

	
	2- 释放一个内存
	
	{{./pasted_image013.png}}
	{{./pasted_image014.png}}



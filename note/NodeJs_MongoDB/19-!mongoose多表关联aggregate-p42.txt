Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-08-05T22:26:48+08:00

====== 19-!mongoose多表关联aggregate-p42 ======
Created 星期三 05 八月 2020

{{.\pasted_image.png}}

=== 1- 建立db.js、 articlecate.js、article.js、user.js ===

		db.js
		{{.\pasted_image001.png}}
		
		articlecate.js:

		{{.\pasted_image002.png}}

		article.js:
		{{.\pasted_image003.png}}
		
		user.js
		{{.\pasted_image004.png}}




=== 2- 分别向articlecate.js、user.js中增加数据，再向article中增加数据 ===
	为了使三个表联系起来，在建立表数据时候：
	article表中的**cid**应该与articlecate中的**_id**相同
	article表中的**author_id**应于user表中的**_id**相同
		{{.\pasted_image008.png}}

=== 3- 进行表的聚合操作 ===

		{{.\pasted_image006.png}}
		{{.\pasted_image007.png}}











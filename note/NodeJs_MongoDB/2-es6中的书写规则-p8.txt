Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-07-27T23:11:14+08:00

====== 2-es6中的书写规则-p8 ======
Created 星期一 27 七月 2020

==== 1- 模板字符串 `  ` ====
		` ${name}` 的使用 

	{{.\pasted_image.png}}
	

==== 2-结构体中方法和属性的简写 ====
		1- 属性的简写：
			原始写法：							属性名和变量明相同时，可以省略：
			{{.\pasted_image002.png}}				{{.\pasted_image003.png}}  

==== 2- 方法简写run函数： ====
			原始写法：									run简写（属性简写）：
				{{.\pasted_image008.png}}     {{.\pasted_image007.png}}

==== 3- 箭头函数 this 指上下文 ====

			正常情况：								es6中箭头函数：
			{{.\pasted_image009.png}}						{{.\pasted_image010.png}}


==== 4- 通过回调函数获取异步函数里面的数据 ====

	1- callback解决方式：
		{{.\pasted_image015.png}}
		
		2- promise处理方式， resolve成功的回调函数，reject失败的回调函数
			{{.\pasted_image016.png}}




3- async 和 await 的用法
	async： 让方法异步，申明异步方法
	await：等待异步执行完成，必须用在async方法里面
		{{.\pasted_image017.png}}

	**直接使用console.log(await test());是错误的**







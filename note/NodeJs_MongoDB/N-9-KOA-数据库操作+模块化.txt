Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-08-09T19:47:42+08:00

====== N-9-KOA-数据库操作+模块化 ======
Created 星期日 09 八月 2020

=== 1- 设置数据库的配置文件 ===
	{{.\pasted_image.png}}


=== 2-定义数据库类，封装方法。 ===

	{{.\pasted_image002.png}}
	{{.\pasted_image003.png}}

	{{.\pasted_image004.png}}

=== 3- 调用数据库函数 ===
	{{.\pasted_image005.png}}





Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-07-29T23:16:34+08:00

====== 6- Get和Post使用-p13 ======
Created 星期三 29 七月 2020

**HTTP协议**的设计目的是保证客户端与服务器端之间的通信，在客户端和服务器之间进行请求-响应时，最常用的方法是：

1- GET：从指定的资源请求数据（一般用去**获取**数据）
2- POST： 向指定的资源提交要被处理的数据（一般用于**提交**数据）

列子：

==== 任务：完成在login页面进行交互，提交用户名和密码。在doLogin页面获取提交过来的数据 ====
			
			
			1- 新建用于交互的form.ejs文件:
					{{.\pasted_image003.png}}
			
			2- 在login业务中绑定form.ejs文件
			{{.\pasted_image001.png}}

3- 在/doLogin中解析post数据
 
						{{.\pasted_image004.png}}





















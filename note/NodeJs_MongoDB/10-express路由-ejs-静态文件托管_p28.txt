Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-07-31T18:44:19+08:00

====== 10-express路由-ejs-静态文件托管 p28 ======
Created 星期五 31 七月 2020

==== 1- 在express中使用ejs模板引擎 ====

		1- 引入express模块
		2-安装ejs。 npm install ejs --save
	
		3-在views目录中创建index.ejs文件（express中应用ejs模板，默认ejs文件放在views目录下）
		4- **三步**进行**路由与ejs文件**和数据的绑定
		
		{{.\pasted_image.png}}

==== 2- 在ejs模板中进行循环遍历和输出操作语句和引入公共底部等 ====

					{{.\pasted_image001.png}}	



==== 3- 在ejs模板里加载一个css样式 ====

		注：css样式一般放在静态资源库下（即static目录下）。
		1- 定义好css文件:
				{{.\pasted_image002.png}}

		2-express.static 托管静态文件
			{{.\pasted_image003.png}}
			
		3- 在ejs模板中引入css文件。
				
				{{.\pasted_image004.png}}










Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-09-22T16:24:16+08:00

====== O-7-CMS-封装公共删除功能-MongoDB数据库导入导出 ======
Created 星期二 22 九月 2020

=== 1- Mongo数据库的操作 ===
		**1- MongoDB数据库的导出：**

			mongodump -h dbhost -d dbname -o dbdirectory

			-h: 主机地址
			-d: 要导出的数据库名字
			-o: 导出目录

		
		**2- MongoDB数据库的导入：**
			
			mongorestore -h dbhost -d dbname path
			
			-h: 主机地址
			-d: 导入数据库的名称（此名称可以于要导入的数据库名称不同）
			path：要导入的数据库的地址
			
			{{.\pasted_image002.png}}
			


=== 2- 跳转到上一页的功能 ===

（比如删除是一个公共的函数，管理员删除，用户删除都是通过这个函数实现。现在我们需要在删除操作完成后，能让页面跳转到对应的页面上，也就是删除管理员后跳回到编辑删除管理员的页面，删除用户后条回到编辑删除用户的页面（从此页面进入的删除页面））

**koa中记录上页地址的功能： ctx.request.headers['referer']**

1- 在admin页面设置全局变量，
		{{.\pasted_image003.png}}



2- 通过redirect跳转到上一个页面去

		{{.\pasted_image004.png}}


=== 3- 设置删除提示，确认后再继续删除（通过js级别实现） ===

	1- 首先再html中为删除标签对应的跳转（<a >）设置 class='delete111'值，用于jquery进行识别操作。默认情况下，会直接跳转到href对应的地址。	（可以将所有删除操作都加上此标签）
			{{.\pasted_image006.png}}
			
	2- 对带 class=‘delete111’ 标签的元素进行jq操作，在basic.js中定义操作函数。
			
			{{.\pasted_image007.png}}
			
	3- 因为这个函数是一旦加载页面后就需要被调用，而不是在某处人为去调动触发，所以只要页面加载此jq，则就需要自动调用次函数，所以在basic.js中开头就直接调用，则html页面中只要引入此js文件，就会对带有class=‘delete111’标签的，进行事件绑定。
	
		{{.\pasted_image008.png}} 

	{{.\pasted_image005.png}}









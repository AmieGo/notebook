Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-07-31T23:03:02+08:00

====== 12-nodejs工程设计-p33 ======
Created 星期五 31 七月 2020

===== 1- 使用Express搭建中大型企业级项目架构 : =====
		{{.\pasted_image.png}}
		


====== 一、手动搭建项目 ======

===== 1- 创建一个空文件夹 =====
				1- npm init --yes     //初始化文件夹啊
				2- 创建3个文件夹和一个入口文件app.js
								**routes文件夹**： 存放路由模块（**每个功能都是一个路由**，即 .js 文件）
								**static（public）文件夹**： 存放静态文件（静态资源文件）css, image文件夹
								**views文件夹**：模板文件(ejs, html)文件
						  **app.js:**   	入口函数

								{{.\pasted_image002.png}}

===== 2- 分析工程树状功能结构图，定义路由框架，并暴露路由。 =====

==== 1- 在router文件夹中创建admin.js(对应后台)   api.js(对应API)   index.js(对应前台PC) ====

==== 2- 创建框架（测试后再写具体功能），以admin.js为列，其余相同 ====
					{{.\pasted_image003.png}}
					
==== 3- 在app.js中进行路由挂载（根） ====
				{{.\pasted_image004.png}}   {{.\pasted_image005.png}}
				
==== 4- 按**层**按**序**设置其他路由：ex：admin路由下还有很多其他路由。则在admin.js文件夹中，建立admin文件夹，将其下的路由全部放在对应文件夹下： ====
						{{.\pasted_image006.png}}   ====>>>>>>{{.\pasted_image007.png}}

==== 5- 按照3中的方法分别写好login.js， nav.js， user.js 的代码框架，并暴露出去（测试是否成功），在admin.js文件（根路由中）中进行路由配置（挂载），完成按层进行路由。 ====

					**子路由**login.js 中写好功能框架并暴露：
					{{.\pasted_image008.png}}
					
					**根路由 admin.js中挂载下一级路由：**
					
					{{.\pasted_image009.png}}
					
					
					{{.\pasted_image010.png}}
					













====== 二 官方提供的express-generator生成项目 ======

全局安装express-generator：

# npm install -g express-generator

执行工程创建：
# express --view=ejs demo-21-express-generator



















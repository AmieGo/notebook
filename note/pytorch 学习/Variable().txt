Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-04-13T18:54:05+08:00

====== Variable() ======
Created 星期六 13 四月 2019
总结： 看以下的两个列子可以明白其用法
		{{./pasted_image002.png}}
	比如我们的权重矩阵w是需要根据反向传播，梯度下降进行更新的，那么我们就把tensor形式的w转化在Variable中存起来，那么w在就具有了3个属性（data本身.data，对应tensor的梯度.grad，说明.grad_fn）
	
	比如上图： 输入为4为向量，然后权重为（4,4）（4,2）的矩阵
						layer1---> layer2 :  输入 x 于 w 家和 获得 out1    （求和方程式1）
						layer2 ：  		out1 在layer2的神经元上经过激活函数处理 获得active_out1 (激活方程式2)
						layer2---->layer3 :  out1 与 w2 相家和获得 out2  (求和返程2)
						layer3：  out2经过激活处理获得active_out2 (激活方程式2)
		然后获得神经网络的输出，将其与label进行对比，获得误差方程
		然后通过链式法则，去求得所有权重此时的梯度值（比如w = 0.222 是的梯度值）
		然后通过权重更新公式去更新w的值
		
		
		Variable的作用就是不用我们去计算，pytorch在定义好神经网络，损失函数，激活函数后。计算图知道求和方程 和 激活方程，损失函数返程，所以他会自己知道要求某个w的求导链式方程，然后保存这些值。






{{./pasted_image.png}}

{{./pasted_image001.png}}

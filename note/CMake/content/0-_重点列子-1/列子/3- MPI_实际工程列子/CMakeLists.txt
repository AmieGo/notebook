cmake_minimum_required(VERSION 3.15.2)

project(Xiaorui)

# add_library()

add_executable(${PROJECT_NAME} hello.cpp)
target_include_directories(${PROJECT_NAME} 
    PUBLIC include/mpi_include/
    #or
    #PUBLIC /home/amie/..../mpi_include/

)

target_link_directories(${PROJECT_NAME} 
    PRIVATE lib/mpi_lib/
    #or
    #PUBLIC /home/amie/..../mpi_lib/

    )


target_link_libraries(${PROJECT_NAME} mpi)

Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2021-05-14T12:01:27+02:00

====== 11-PUBLIC PRIVATE INTERFACE ======
Created 星期五 14 五月 2021


本文旨在搞懂PUBLIC，PRIVATE， INTERFACE的用法。
https://kubasejdak.com/modern-cmake-is-like-inheritance  

**1- 首先需要弄清一个概念，在parent directory中的CMakeLIsts.txt中使用target_link_libraries()中目标在链接其他的库的时候，其他库中已经包含了自身的头文件的路径，我们不需要在parent directory中的CMakeLists.txt中再次申明要链接库的头文件的路径。**

	列子： 项目的目路结构如下
			{{./pasted_image.png}}	
	
	
	逻辑关系： 
		1- 构建integrate lib需要依赖add_func和sub_func和pub （integrate目录下的CMakeLIsts.txt）
				{{./pasted_image002.png}}

		2- 在main.cpp中想使用pub.cpp中的函数，那么在已经成功编译integrate lib的情况下，我们不需要在
		（4-PUBLIC_INTERFACE_PRIVATE目录下的CMakeLists.txt中再次指明pub.h的路径, 
			**直接在main.cpp中#include "pub.h"**即可，编译器会通过编译好的integrate lib找到头文件pub.h的位置）
				
				如果我们不想main.cpp访问add.cpp中的函数（安全性），那么就在编译integrate lib的时候将add.h的路径设置为PRIVATE，
				那么编译器在将integrate lib链接到main的时候，add.h就对main.cpp透明（main.cpp无法访问add.h），也就是说不能像引入pub.h一样直接在main.cpp中#include"add.h"就可以使用add.cpp中的函数了。

				**注意！！！：**
				如果此时在main.cpp目录下的CMakeLists中，指明add.h的路径，**那么在编译integrate lib中**
				**对add.h设置的私有访问属性就无效了**，main.cpp此时可以
				直接去访问add.h（直接#include"add.h"）。这是我们不想要的效果，
				我们希望在链接integrate lib的时候，add.h是main.cpp 无法访问的私有成员。
				
				**所以不要在 父CMakeLists中去设置子CMakeLists需要的headerde 路径，保持CMakeLists每层都只负责自己的层，**
				**只在自己的层工作**
				
				{{./pasted_image003.png}}


				**注意2：**
				当使用target_link_libraries(main **PUBLIC** integrate)的时候 那么当其他的项目（ex，root项目）要链接main库的时候，integrate中的PUBLIC，PRIVATE，INTERFACE对于root项目来说不会改变。
				
				但当使用target_link_libraries(main **PRIVATE** integrate) 的时候 那么integrate中的PUBLIC，PRIVATE，INTERFACE对于root来说就**全部变为PRIVATE**属性了
				
				
				
				


==== 2- PRIVATE 和 PUBLIC的使用列子。 ====

		https://bitbucket.org/AmieGo/cmake_practice/src/master/4-PUBLIC_INTERFACE_PRIVATE/
		
		4-PUBLIC_INTERFAC_PRIVATE项目（其库的层级结构，和继承关系如下）
				{{./pasted_image005.png}}




==== 3- INTERFACE(重要特点： 不产生二进制文件) ====
https://kubasejdak.com/modern-cmake-is-like-inheritance

http://mariobadr.com/creating-a-header-only-library-with-cmake.html

	{{./pasted_image006.png}}
	{{./pasted_image007.png}}\
	
	{{./pasted_image008.png}}







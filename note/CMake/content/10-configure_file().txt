Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2021-05-12T11:07:56+02:00

====== 10-configure file() ======
Created 星期三 12 五月 2021

0- 很好的列子。
	https://github.com/xiaoruiDu/cmake-examples/tree/master/03-code-generation/configure-files
	
	其中.h file是根据需要由.h.in文件生成的，的而不是开始就存在在工程中的。其目录结构如下
	
	{{./pasted_image005.png}}
	
	一个是${} syntax  语法 （默认）
	一个是@@ syntax 语法


1- 根据CMakeLIsts的不同编译环境生成不同的.h 头文件
		https://stackoverflow.com/questions/8887117/can-cmake-generate-a-configure-file
		ex： 应用场景：
		
				在计算机有gpu的时候允许cuda库，没有gpu的时候禁止cuda库。
				这就应该对应两种类型的头文件（cpu/gpu version），使用configure_file（）可以根据不同的配置生成合适的头文件。
				
				


列子：

		{{./pasted_image002.png}}
		{{./pasted_image004.png}}
		{{./pasted_image003.png}}

		{{./pasted_image.png}}
		{{./pasted_image001.png}}


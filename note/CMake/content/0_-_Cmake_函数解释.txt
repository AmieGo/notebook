Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2021-03-13T15:33:20+01:00

====== 0 - Cmake 函数解释 ======
Created 星期六 13 三月 2021

1- **cmake_minimum_required**(VERSION 3.15.2)

2- **project**(Xiaorui)

3- **add_executable**(${PROJECT_NAME} main.cpp)

4- **add_library**(adderx adder.h adder.cpp)

	# 生成静态文件或者动态文件（具体要看命令，以上默认生成为静态文件 'libadderx.a' ）
 
4- **target_include_directories**(${PROJECT_NAME} 
	PUBLIC Adder
	PUBLIC include
	)

5- **target_link_directories**(${PROJECT_NAME} 
	PRIVATE ./
	PRIVATE ./src/
	)


6- **target_link_libraries**(${PROJECT_NAME} adderx glfw)



**7- add_subdirectory( )**
	其次级目录路径中含CMakeLists.txt，和所有源文件(.cpp), 一旦加入此命令，那么上一级的CMakeLists.txt 编译到这里的时候就会根据次级
	目录的CMakeLists.txt转而去编译次级目录的源文件。（可以通过message() 打印编译，其编译顺序）



	






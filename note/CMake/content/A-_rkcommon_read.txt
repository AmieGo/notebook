Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2021-05-22T11:21:42+02:00

====== A- rkcommon read ======
Created 星期六 22 五月 2021

===== CMakeList.txt structure (Intel style) =====


**1- 学会写macro**
	其和function的区别就在于scope上，macro是全局的，scope需要指定PARENT_SCOPE所以定义全局的变量就用macro写，
	其语法如下
	
	**macro**( 函数名字 参数(可没有) )
		content
	**endmacro()**
	
	{{./pasted_image.png}}
	
	
	凡是在macro中定义过的参数其上一级的菜单中也是可以访问的。比如：在第一level的CMakeLists.txt中调用了rkcommon_macrps.cmake中的函数rkcommon_configure_build_type() macro。然后在此macro中定制了变量RKCOMM_FLAG, 那么在此CMakeLists.txt中也可以直接访问RKCOMM_FLAG 变量。
	
	如果使用function的话就不行，除非用关键字**PARENT_SCOPE**明确指出来
	
	{{./pasted_image001.png}}
	
	


2- 
{{./pasted_image002.png}}
{{./pasted_image003.png}}
{{./pasted_image004.png}}
{{./pasted_image005.png}}

{{./pasted_image006.png}}



{{./pasted_image008.png}}

{{./pasted_image009.png}}
{{./pasted_image010.png}}
{{./pasted_image011.png}}
1- Global options（）









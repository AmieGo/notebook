Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2021-03-03T13:28:59+01:00

====== 2-Levels ======
Created 星期三 03 三月 2021

**1- makefile是十分复杂的，如下图。但是我们并不需要自己手写，我们只需要写CMakeLists.txt制定依赖关系，source code位置，编译目录等信息就可以用cmake生成makefile文件**

{{./pasted_image.png}}


**2- 创建一个空CMakeLists.txt，一个main.cpp, 一个build folder 然后使用cmake -S .. -B . 命令，即可生成Makefile等文件在build 文件夹中。**

**3-  填写内容到CMakeLists.txt in vscode.**

{{./pasted_image002.png}}
	
	3-1 然后在build目录中执行 **#cmake -S ../ -B .**
		-S: 后面接main.cpp所在的目录
		-B：后面接编译完后输出的路径目录（ex：build中）
	
	3-2 然后在build目录中执行 **# make，**就会在build目录中生成Xiaorui的可执行文件，直接运行即可输出。
			然后执行 **# ./Xiaorui **即可输出结果





import socket
import struct
from threading import Thread

IP = '127.0.0.1'
PORT = 65432


def recvall(count,con):
    buf = b''
    while count:
        newbuf = con.recv(count)
        if not newbuf:
            return None
        buf += newbuf
        count -= len(newbuf)
    return buf


class Server(Thread):
    def __init__(self, con, address):

        super(Server, self).__init__()
        self.con = con
        self.address =address

    def run(self):
        try:
            while 1:
                #每条信息取两次，第一次取长度，第二次更具长度提取真实数据。
                #step1: 获得信息长度的二进制编码，并用struct.unpack()解码，获得真实信息长度
                buf = recvall(2,self.con)  # 获得msg长度信息的二进制编码（使用struct.pack()实现）
                length, = struct.unpack('h', buf) # 解码获得msg长度
                #step2： 根据length 获得真实信息
                msg = recvall(length,con)       # 从buffer中回去真正msg
                print(msg)

                if msg ==b'imu':
                    print('do something for imu data')

                    send_back_msg = b'alread processed the imu data'
                    s_b_length = len(send_back_msg)
                    print(s_b_length)
                    con.sendall(struct.pack('h', s_b_length))
                    con.sendall(send_back_msg)
                else:
                    print('no imu data.')
                    send_back_msg = b'imu data is still required'
                    s_b_length = len(send_back_msg)
                    print(s_b_length)
                    con.sendall(struct.pack('h', s_b_length))
                    con.sendall(send_back_msg)

        except Exception as e:
            print(e)




if __name__ == '__main__':

    server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server.bind((IP, PORT))
    server.listen(1)
    print('Listening on {}:{}'.format(IP, PORT))
    # con, address = server.accept()

    while 1:
        con, address = server.accept()
        print(address)

        thread1 = Server(con, address)
        thread1.start()


        # buf = recvall(2,con)  # 获得msg长度信息的二进制编码（使用struct.pack()实现）
        # length, = struct.unpack('h', buf) # 解码获得msg长度
        # msg = recvall(length,con)       # 从buffer中回去真正msg
        # print(msg)
        #
        # send_back_msg = b'dxr_niu_bi'
        # s_b_length = len(send_back_msg)
        # print(s_b_length)
        # con.sendall(struct.pack('h', s_b_length))
        # con.sendall(send_back_msg)

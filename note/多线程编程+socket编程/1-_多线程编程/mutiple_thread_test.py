from threading import Thread
import time
import threading

data = [0,0]
num = 0

mutex = threading.Lock()

class Mythread(Thread):
    def __init__(self):
        super(Mythread, self).__init__()

    # # 不加锁列子
    # def run(self):
    #     global data
    #     global flag
    #     count = 0
    #
    #     while count < 100:
    #
    #         if Mythread.getName(self) == 'thread1':
    #             # mutex.acquire()
    #             data[0] += 1
    #             # time.sleep(1)
    #             print('thread_1 mutex:', data)
    #             # mutex.release()
    #
    #         elif Mythread.getName(self) == 'thread2':
    #             # mutex.acquire()
    #             data[1] = data[0]
    #             print('thread_2 mutex:', data)
    #             time.sleep(2)
    #             data[1] = data[0]
    #             print('thread_2 mutex_delay:', data)
    #             time.sleep(2)
    #             # mutex.release()
    #
    #         count += 1


    # 加锁列子
    def run(self):
        global data
        global flag
        count = 0

        while count < 100:

            if Mythread.getName(self) == 'thread1':
                mutex.acquire()
                data[0] += 1
                # time.sleep(1)
                print('thread_1 mutex:', data)
                mutex.release()

            elif Mythread.getName(self) == 'thread2':
                mutex.acquire()
                data[1] = data[0]
                print('thread_2 mutex:', data)
                time.sleep(2)
                data[1] = data[0]
                print('thread_2 mutex_delay:', data)
                time.sleep(2)
                mutex.release()

            count += 1



if __name__ == '__main__':
    thread1 = Mythread()
    thread1.setName('thread1')
    thread2 = Mythread()
    thread2.setName('thread2')

    thread1.start()
    thread2.start()

    # print(data)





#
# from threading import Thread
# import time
# num = 0
#
# def add_num():
#     global num
#     for i in range(100000):
#         num += 1
# if __name__ == '__main__':
#     t1 = Thread(target=add_num)
#     t2 = Thread(target=add_num)
#     t3 = Thread(target=add_num)
#     t1.start()
#     t2.start()
#     t3.start()
#     #time.sleep(1)
#     print(num)
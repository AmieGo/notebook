import socket
import struct
import time

import threading as Thread


HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 65432        # The port used by the server


#class MyThread(Thread):
#    def __init__(self):
#        super(MyThread, self).__init__()
#    
#    def run(self):
#        pass
#        
        
    


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    
    s.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    s.connect((HOST, PORT))
    
    while 1:
        
        # 先发送信息类型 （含byte化后的 信息长度+信息）
        msg_request = b'imu'
        msg_request_length = struct.pack('h',len(msg_request))
        complete_msg = msg_request_length + msg_request # 二进制信息
        s.sendall(complete_msg)
        
        #再发送真实的数据（含byte化后的 信息长度+信息）
        msg_truely = b'201721008409'
        msg_truely_length = struct.pack('h',len(msg_truely))
        complete_truely_msg = msg_truely_length + msg_truely #二进制信息
        s.sendall(complete_truely_msg)
        
        # 收到client 端发回来的对应的respond
        res_data_length_byte = s.recv(2)
        res_data_length, = struct.unpack('h',res_data_length_byte)
        res_data_byte = s.recv(res_data_length)
        print('Received', repr(res_data_byte))
        
#        # 先发送信息类型 （含byte化后的 信息长度+信息）
        msg_request = b'dxr'
        msg_request_length = struct.pack('h',len(msg_request))
        complete_msg = msg_request_length + msg_request # 二进制信息
        s.sendall(complete_msg)
        
        #再发送真实的数据（含byte化后的 信息长度+信息）
        msg_truely = b'dxr201721008409'
        msg_truely_length = struct.pack('h',len(msg_truely))
        complete_truely_msg = msg_truely_length + msg_truely #二进制信息
        s.sendall(complete_truely_msg)
#        
        
        # 收到client 端发回来的对应的respond
        res_data_length_byte_2 = s.recv(2)
        res_data_length_2, = struct.unpack('h',res_data_length_byte_2)
        res_data_byte_2 = s.recv(res_data_length_2)
        print('Received', repr(res_data_byte_2))


    







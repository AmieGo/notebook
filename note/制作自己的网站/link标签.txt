Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-07-22T21:43:16+08:00

====== link标签 ======
Created 星期三 22 七月 2020

==== 1-<link>标签定义及使用说明 ====

	<link> 标签定义文档与外部资源的关系。
	<link> 标签最常见的用途是链接样式表。
	注意： link 元素是**空元素**，它仅包含属性。
	注意： 此元素只能存在于 head 部分，不过它可出现任何次数。

==== 2- 关键字的使用 ====
	{{.\pasted_image.png}}
	{{.\pasted_image001.png}}
	{{.\pasted_image002.png}}
==== 3- rel 关键字的作用的解释 ====
		{{.\pasted_image003.png}}

3- 

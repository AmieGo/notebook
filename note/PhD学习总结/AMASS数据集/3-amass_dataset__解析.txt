Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-10-28T16:11:17+01:00

====== 3-amass dataset  解析 ======
Created 星期三 28 十月 2020

https://github.com/nghorbani/amass/blob/master/notebooks/01-AMASS_Visualization.ipynb


1- A single data file in amass has the parameters to control **gender**, **pose**, **shape**, **global translation** and **soft tissue dynamics** in correspondence with the original motion capture sequence. 

	amass dataset数据集重要数据：
		
		**Vector poses has 156** elements for each of 693 frames.
		**Vector dmpls has 8** elements for each of 693 frames.
		**Vector trams has 3** elements for each of 693 frames.
		**Vector betas has 16** elements constant for the whole sequence.
		The subject of the mocap sequence is **male**.

2- 可视化amassdataset(只针对**smplh模型**)

https://github.com/nghorbani/amass/blob/master/notebooks/01-AMASS_Visualization.ipynb
	**test_visualization.py**  dataloader，设计pytoch的dataloader加载的数据包含：**root_orient (3d),  pose_body (63d),  pose_hand(90d), betas(16)**
	{{./pasted_image.png}}

	
	
	可视化amassdataset，根据输出需要，选择smplh模型的amassdata输入值：

	1- ** pose_body (63d), 和 betas(16)，输入为（6890，3）的vertices数据，用于模型的渲染**
	
	{{./pasted_image004.png}}
	
{{./pasted_image003.png}}
	
	{{./pasted_image006.png}}
	
{{./pasted_image007.png}}

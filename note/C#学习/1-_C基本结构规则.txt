Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2020-09-28T22:37:26+08:00

====== 1- C基本结构规则 ======
Created 星期一 28 九月 2020

===== 0- 基本知识： =====

=== 1- 必须以分号（ ; ）结尾。 ===

=== 2- 编译 & 执行 C# 程序 ===

			1- 如果您使用 **Visual Studio.Net** 编译和执行 C# 程序，请按下面的步骤进行：

				启动 Visual Studio。
				在菜单栏上，选择 File -> New -> Project。
				从模板中选择 Visual C#，然后选择 Windows。
				选择 Console Application。
				为您的项目制定一个名称，然后点击 OK 按钮。
				新项目会出现在解决方案资源管理器（Solution Explorer）中。
				在代码编辑器（Code Editor）中编写代码。
				点击 Run 按钮或者按下 F5 键来运行程序。会出现一个命令提示符窗口（Command Prompt window），显示 Hello World。
				
			2- 您也可以使**用命令行**代替 Visual Studio IDE 来编译 C# 程序：

				打开一个文本编辑器，添加上面提到的代码。
				保存文件为 helloworld.cs。
				打开命令提示符工具，定位到文件所保存的目录。
				键入 csc helloworld.cs 并按下 enter 键来编译代码。
				如果代码没有错误，命令提示符会进入下一行，并生成 helloworld.exe 可执行文件。
				接下来，键入 helloworld 来执行程序。
				您将看到 "Hello World" 打印在屏幕上。

=== 1- 基本语法 ===

		1- 结构
		
			{{.\pasted_image.png}}
			{{.\pasted_image001.png}}
























